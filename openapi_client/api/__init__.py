from __future__ import absolute_import

# flake8: noqa

# import apis into api package
from openapi_client.api.authentication_api import AuthenticationApi
from openapi_client.api.cascade_core_api import CascadeCoreApi
from openapi_client.api.gate_api import GateApi
from openapi_client.api.power_api import PowerApi
from openapi_client.api.reservoir_api import ReservoirApi
from openapi_client.api.reservoir_core_api import ReservoirCoreApi
from openapi_client.api.run_of_river_core_api import RunOfRiverCoreApi
from openapi_client.api.secondary_api import SecondaryApi
