# openapi_client.ReservoirApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_reservoir_request_level**](ReservoirApi.md#api_reservoir_request_level) | **GET** /plant/{plant_id}/reservoir/level | Get reservoir level timeseries
[**api_reservoir_submit_level**](ReservoirApi.md#api_reservoir_submit_level) | **POST** /plant/{plant_id}/reservoir/level | Submit reservoir level timeseries for a reservoir


# **api_reservoir_request_level**
> object api_reservoir_request_level(plant_id, start_ts, end_ts)

Get reservoir level timeseries

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.ReservoirApi(openapi_client.ApiClient(configuration))
plant_id = 'plant_id_example' # str | The id of the plant
start_ts = 56 # int | Start of query
end_ts = 56 # int | End of query

try:
    # Get reservoir level timeseries
    api_response = api_instance.api_reservoir_request_level(plant_id, start_ts, end_ts)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReservoirApi->api_reservoir_request_level: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plant_id** | **str**| The id of the plant | 
 **start_ts** | **int**| Start of query | 
 **end_ts** | **int**| End of query | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Reservoir level timeseries |  -  |
**400** | Invalid parameter value in the request |  -  |
**401** | Access token is missing or invalid |  -  |
**404** | Unsupported parameter in the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_reservoir_submit_level**
> api_reservoir_submit_level(plant_id, request_body)

Submit reservoir level timeseries for a reservoir

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.ReservoirApi(openapi_client.ApiClient(configuration))
plant_id = 'plant_id_example' # str | The id of the plant
request_body = None # list[object] | Timeseries to submit

try:
    # Submit reservoir level timeseries for a reservoir
    api_instance.api_reservoir_submit_level(plant_id, request_body)
except ApiException as e:
    print("Exception when calling ReservoirApi->api_reservoir_submit_level: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plant_id** | **str**| The id of the plant | 
 **request_body** | [**list[object]**](object.md)| Timeseries to submit | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | reservoir level successfully submitted |  -  |
**400** | Invalid parameter value in the request |  -  |
**401** | Access token is missing or invalid |  -  |
**404** | Unsupported parameter in the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

