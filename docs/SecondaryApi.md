# openapi_client.SecondaryApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_gate_request_discharge_actual**](SecondaryApi.md#api_gate_request_discharge_actual) | **GET** /plant/{plant_id}/gate/discharge-actual | Get gate discharge timeseries
[**api_power_request_active_gross_actual**](SecondaryApi.md#api_power_request_active_gross_actual) | **GET** /plant/{plant_id}/power/active-gross-actual | Get the actual power of a plant
[**api_reservoir_request_level**](SecondaryApi.md#api_reservoir_request_level) | **GET** /plant/{plant_id}/reservoir/level | Get reservoir level timeseries


# **api_gate_request_discharge_actual**
> object api_gate_request_discharge_actual(plant_id, start_ts, end_ts)

Get gate discharge timeseries

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.SecondaryApi(openapi_client.ApiClient(configuration))
plant_id = 'plant_id_example' # str | The id of the plant
start_ts = 56 # int | Start of query
end_ts = 56 # int | End of query

try:
    # Get gate discharge timeseries
    api_response = api_instance.api_gate_request_discharge_actual(plant_id, start_ts, end_ts)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecondaryApi->api_gate_request_discharge_actual: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plant_id** | **str**| The id of the plant | 
 **start_ts** | **int**| Start of query | 
 **end_ts** | **int**| End of query | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | gate discharge timeseries |  -  |
**400** | Invalid parameter value in the request |  -  |
**401** | Access token is missing or invalid |  -  |
**404** | Unsupported parameter in the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_power_request_active_gross_actual**
> object api_power_request_active_gross_actual(plant_id, start_ts, end_ts)

Get the actual power of a plant

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.SecondaryApi(openapi_client.ApiClient(configuration))
plant_id = 'plant_id_example' # str | The id of the plant
start_ts = 56 # int | Start of query
end_ts = 56 # int | End of query

try:
    # Get the actual power of a plant
    api_response = api_instance.api_power_request_active_gross_actual(plant_id, start_ts, end_ts)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecondaryApi->api_power_request_active_gross_actual: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plant_id** | **str**| The id of the plant | 
 **start_ts** | **int**| Start of query | 
 **end_ts** | **int**| End of query | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | actual power of a plant |  -  |
**400** | Invalid parameter value in the request |  -  |
**401** | Access token is missing or invalid |  -  |
**404** | Unsupported parameter in the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_reservoir_request_level**
> object api_reservoir_request_level(plant_id, start_ts, end_ts)

Get reservoir level timeseries

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.SecondaryApi(openapi_client.ApiClient(configuration))
plant_id = 'plant_id_example' # str | The id of the plant
start_ts = 56 # int | Start of query
end_ts = 56 # int | End of query

try:
    # Get reservoir level timeseries
    api_response = api_instance.api_reservoir_request_level(plant_id, start_ts, end_ts)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SecondaryApi->api_reservoir_request_level: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plant_id** | **str**| The id of the plant | 
 **start_ts** | **int**| Start of query | 
 **end_ts** | **int**| End of query | 

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Reservoir level timeseries |  -  |
**400** | Invalid parameter value in the request |  -  |
**401** | Access token is missing or invalid |  -  |
**404** | Unsupported parameter in the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

