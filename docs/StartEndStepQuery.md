# StartEndStepQuery

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_ts** | **int** | Unix timestamp in milliseconds, should be &gt;&#x3D; 1262304000000 (2010-01-01 00:00:00) | 
**end_ts** | **int** | Unix timestamp in milliseconds, should be &gt;&#x3D; 1262304000000 (2010-01-01 00:00:00) | 
**step_size** | **int** | a time interval (typically used as step-size) in milliseconds. for hourly use &#39;3600000&#39; for 15min use &#39;900000&#39; | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


