# GateOpeningWithModeTimeseriesElem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**timestamp** | **int** | Unix timestamp in milliseconds, should be &gt;&#x3D; 1262304000000 (2010-01-01 00:00:00) | 
**value** | **float** | Gate opening in cm | 
**mode** | [**OperationMode**](OperationMode.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


