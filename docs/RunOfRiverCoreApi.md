# openapi_client.RunOfRiverCoreApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_power_submit_active_gross_actual**](RunOfRiverCoreApi.md#api_power_submit_active_gross_actual) | **POST** /plant/{plant_id}/power/active-gross-actual | Submit power timeseries for a whole plant


# **api_power_submit_active_gross_actual**
> api_power_submit_active_gross_actual(plant_id, request_body)

Submit power timeseries for a whole plant

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.RunOfRiverCoreApi(openapi_client.ApiClient(configuration))
plant_id = 'plant_id_example' # str | The id of the plant
request_body = None # list[object] | Timeseries to submit

try:
    # Submit power timeseries for a whole plant
    api_instance.api_power_submit_active_gross_actual(plant_id, request_body)
except ApiException as e:
    print("Exception when calling RunOfRiverCoreApi->api_power_submit_active_gross_actual: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **plant_id** | **str**| The id of the plant | 
 **request_body** | [**list[object]**](object.md)| Timeseries to submit | 

### Return type

void (empty response body)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | power successfully submitted |  -  |
**400** | Invalid parameter value in the request |  -  |
**401** | Access token is missing or invalid |  -  |
**404** | Unsupported parameter in the request. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

