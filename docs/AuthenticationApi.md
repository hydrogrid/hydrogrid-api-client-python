# openapi_client.AuthenticationApi

All URIs are relative to *https://api.hydrogrid.eu/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**api_auth_get_access_token**](AuthenticationApi.md#api_auth_get_access_token) | **GET** /auth | Get JWT
[**api_auth_get_refresh_token**](AuthenticationApi.md#api_auth_get_refresh_token) | **POST** /auth/refresh | Refresh JWT


# **api_auth_get_access_token**
> object api_auth_get_access_token()

Get JWT

Main Authentication endpoint. Supply Basic Authentication credentials to get a JWT. Alternatively the credentials can be passed in the request body 

### Example

* Basic Authentication (basicAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure HTTP basic authorization: basicAuth
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.AuthenticationApi(openapi_client.ApiClient(configuration))

try:
    # Get JWT
    api_response = api_instance.api_auth_get_access_token()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthenticationApi->api_auth_get_access_token: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | token and refresh token |  -  |
**401** | Incorrect username or password |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **api_auth_get_refresh_token**
> object api_auth_get_refresh_token()

Refresh JWT

When your jwt expired use this to get a new one by supplying the expired token and the refresh token

### Example

* Bearer (JWT) Authentication (bearerAuth):
```python
from __future__ import print_function
import time
import openapi_client
from openapi_client.rest import ApiException
from pprint import pprint
configuration = openapi_client.Configuration()
# Configure Bearer authorization (JWT): bearerAuth
configuration.access_token = 'YOUR_BEARER_TOKEN'

# Defining host is optional and default to https://api.hydrogrid.eu/v1
configuration.host = "https://api.hydrogrid.eu/v1"
# Create an instance of the API class
api_instance = openapi_client.AuthenticationApi(openapi_client.ApiClient(configuration))

try:
    # Refresh JWT
    api_response = api_instance.api_auth_get_refresh_token()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthenticationApi->api_auth_get_refresh_token: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

**object**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | access token |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

